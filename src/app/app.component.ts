import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nombre: string = 'Daniel Sáez';
  nombre2: string = 'DaNiEl SAeZ cAsES';
  arreglo = [1,2,3,4,5,6,7,8,9,10];
  PI: number = Math.PI;
  porcentaje: number = 0.234;
  salario: number = 1234.5;
  fecha: Date = new Date();
  idioma: string = 'es';
  videoUrl: string = 'https://www.youtube.com/embed/yrFYVd8BiL8';
  activar: boolean = true;

  valorPromesa = new Promise<string>((resolve) => {
    setTimeout(() => {
      resolve('LLEGO EL BIGOTES!!')
    }, 4500)
  })
  valorPromesaObj = new Promise<{ calle: string, numero: number}>((resolve) => {
    setTimeout(() => {
      resolve({
        calle: 'Juncos',
        numero: 3
      })
    }, 4500)
  })

  persona = {
    nombre: 'Daniel',
    edad: 28,
    clave: 'DSC154',
    direccion: {
      calle: 'Juncos',
      numero: 3
    }
  }
}
